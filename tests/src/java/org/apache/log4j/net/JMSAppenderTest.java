package org.apache.log4j.net;

import junit.framework.TestCase;
import org.apache.log4j.LogManager;

public class JMSAppenderTest extends TestCase {

    public JMSAppenderTest(final String testName) { super(testName); }

    /**
     * Resets configuration after every test.
     */
    public void tearDown() {
        LogManager.resetConfiguration();
    }

    /**
     * Test ensure simple topic names are resolved without exception.
     */
    public void testJNDILookupOfTopicIsAllowed() {
        JMSAppender appender = new JMSAppender();

        appender.throwExceptionIfExternalJNDILookupFound("Topic");
        appender.throwExceptionIfExternalJNDILookupFound("topic-blah_.Blah");
        appender.throwExceptionIfExternalJNDILookupFound("java:com/eg/TopicClass");
    }

    /**
     * Test ensure topic names with are resolved without exception.
     */
    public void testJNDILookupOfTopicIsDisallowed() {
        JMSAppender appender = new JMSAppender();

        try {
            appender.throwExceptionIfExternalJNDILookupFound("ldap:com/eg/Class");
            fail("Expected invalid character exception thrown");
        } catch (RuntimeException re) {
            //
        }

        try {
            appender.throwExceptionIfExternalJNDILookupFound("http://google.com.au");
            fail("Expected invalid character exception thrown");
        } catch (RuntimeException re) {
            //
        }

        try {
            appender.throwExceptionIfExternalJNDILookupFound("java:ldap:com");
            fail("Expected invalid character exception thrown");
        } catch (RuntimeException re) {
            //
        }
    }
}

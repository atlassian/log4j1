A fork of log4j1 that improves performance.

log4j1 is effectively dead, but getting Atlassian software off log4j1 is herculean effort.  

In the mean time we can improve some of the performance hurdles in an other wise sturdy library that has served us so well.

The last release of was 1.2.17 and hence master starts from there in terms of the upstream repo.

Since log4j1 is no longer maintained, no upstream changes are expected to be seen nor do we expect to push changes back to Apache since its not going to be released.
